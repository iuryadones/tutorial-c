# C Extension with Numpy

Example of the code

## Setup

```bash
$ python -V
Python 3.7.12
$ pip install numpy==1.21.6
```

## Build

```bash
$ python setup.py install
```

## Code

> file: run.py

```python
import capi
import numpy as np

zeros = numpy.zeros((2,2))
result = capi.rank(zeros)
print(result)
```

## Run

```bash
$ python run.py
2
```

## Reference
  - [My first C Extension To Numpy]()
