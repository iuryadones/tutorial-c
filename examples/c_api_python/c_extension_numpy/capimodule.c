#include <Python.h>
#include <numpy/arrayobject.h>

static PyObject*
rank(PyObject *self, PyObject *args)
{
    PyObject *arg1=NULL;
    PyObject *arr1=NULL;
    int nd;

    if (!PyArg_ParseTuple(args, "O", &arg1))
        return NULL;

    arr1 = PyArray_FROM_OTF(arg1, NPY_DOUBLE, NPY_IN_ARRAY);

    if (arr1 == NULL)
        return NULL;

    // char *ptr = PyArray_BYTES(arr1);
    // printf("adress[char]: %p\n", &ptr);
    // printf("adress[PyObject]: %p\n", &arr1);

    nd = PyArray_NDIM(arr1);

    Py_DECREF(arr1);

    return Py_BuildValue("i", nd);
}

static struct PyMethodDef methods[] = {
    {"rank", (PyCFunction) rank, METH_VARARGS, "descript of rank"},
    {NULL, NULL, 0, NULL}
};

#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef cModPyDem = {
    PyModuleDef_HEAD_INIT,
    "rank", "descript of rank",
    -1,
    methods
};

PyMODINIT_FUNC
PyInit_capi(void)
{
    PyObject *module;
    module = PyModule_Create(&cModPyDem);
    if (module==NULL) return NULL;
    if (PyErr_Occurred()) return NULL;
    import_array();
    return module;
}
#else
PyMODINIT_FUNC
initcapi(void)
{
    PyObject *module;
    module = Py_InitModule("capi", methods, "a module");
    if (module==NULL) return NULL;
    import_array();
    return;
}
#endif
