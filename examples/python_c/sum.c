#ifdef SUM_RES_INT_ARGS_INT_INT
int res;
int sum(int x, int y);
int sum(int x, int y) {
    int res = x + y;
    return res;
}
#endif

#ifdef SUM_RES_FLOAT_ARGS_FLOAT_FLOAT
float res;
float sum(float x, float y);
float sum(float x, float y) {
    float res = x + y;
    return res;
}
#endif

#ifdef SUM_RES_FLOAT_POINTER_ARGS_FLOAT_FLOAT
float res;
float* sum(float x, float y);
float* sum(float x, float y) {
    res = x + y;
    return &res;
}
#endif

#ifdef SUM_RES_FLOAT_POINTER_ARGS_FLOAT_ARRAY_FLOAT_ARRAY
float res[3];
float* sum(float x[3], float y[3]);
float* sum(float x[3], float y[3]) {
    for (int i=0; i<3; i++) {
        res[i] = x[i] + y[i];
    }
    return &res[0];
}
#endif
