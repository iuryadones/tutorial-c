import sys
import ctypes as ctps

# LoadLibrary libsum.so
lib = ctps.cdll.LoadLibrary("./libsum.so")
lsum = lib.sum

macro = "-DSUM_RES_INT_ARGS_INT_INT"
if sys.argv[1] == macro:
    # Interger: Types of Args and Result
    lsum.restype = ctps.c_int
    lsum.argtypes = ctps.c_int, ctps.c_int
    args = (1, 2)

macro = "-DSUM_RES_FLOAT_ARGS_FLOAT_FLOAT"
if sys.argv[1] == macro:
    # Float: Types of Args and Result
    lsum.restype = ctps.c_float
    lsum.argtypes = ctps.c_float, ctps.c_float
    args = (1.3, 1.7)

macro = "-DSUM_RES_FLOAT_POINTER_ARGS_FLOAT_FLOAT"
if sys.argv[1] == macro:
    # Float Pointer: Types of Args and Result
    lsum.restype = ctps.POINTER(ctps.c_float)
    lsum.argtypes = ctps.c_float, ctps.c_float
    args = (1.3, 1.7)

macro = "-DSUM_RES_FLOAT_POINTER_ARGS_FLOAT_ARRAY_FLOAT_ARRAY"
if sys.argv[1] == macro:
    # Array Float Pointer: Types of Args and Result
    lsum.restype = ctps.POINTER(ctps.c_float * 3)
    lsum.argtypes = (ctps.c_float*3), (ctps.c_float*3)
    args = (
        (ctps.c_float*3)(1.3, 1.7, 2.1),
        (ctps.c_float*3)(1.7, 1.3, 0.9)
    )

if len(sys.argv) == 2:
    # Execution
    res = lsum(*args)

    # Show logs of the exec
    print(type(res))
    print(res)
    if hasattr(res, "contents"):
        print(res.contents)
        if hasattr(res.contents, "value"):
            print(res.contents.value)
        not_instances = (ctps.c_int, ctps.c_float)
        if not isinstance(res.contents, not_instances):
            n_values = len(res.contents)
            print(n_values)
            for i in range(n_values):
                if i<(n_values-1):
                    print(res.contents[i], end=", ")
                else:
                    print(res.contents[i])
