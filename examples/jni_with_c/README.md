# Hello with JNI used C/C++

Example as used jni with c/c++ and cmake

## Run

```bash
$ ./mk_build.sh
```

## References:
 - (Java Native Interface (JNI))[https://www3.ntu.edu.sg/home/ehchua/programming/java/javanativeinterface.html], Accessed 19 July 2021. 
 - (Amostra: hello-jni)[https://developer.android.com/ndk/samples/sample_hellojni], Accessed 19 July 2021.
