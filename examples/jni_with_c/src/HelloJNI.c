// Save as "HelloJNI.c"
#include <jni.h>        // JNI header provided by JDK
#include <stdio.h>      // C standard IO header
#include "HelloJNI.h"   // Generated


// Implementation of the native method sayHello()
JNIEXPORT void JNICALL
Java_HelloJNI_sayHello(
        JNIEnv *env,
        jobject obj) {
    printf("Hello world!\n");
    printf("Running C code with JDK!\n");
    return;
}

// Implementation of the native method sayHello()
JNIEXPORT jdouble JNICALL
Java_HelloJNI_sum(
        JNIEnv *env,
        jobject obj,
        jdouble x,
        jdouble y) {
    jdouble result;
    result = x + y;
    return result;
}

