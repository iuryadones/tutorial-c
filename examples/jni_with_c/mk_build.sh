#!/bin/bash


# params
PATH_BUILD=$PWD/build
PATH_CACHE=$PATH_BUILD/CMakeCache.txt
PATH_SRC=$PWD/src
PATH_CODE_C=$PATH_SRC/HelloJNI.c

# run script the mk_build in the java_home
PATH_JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

# build .h
javac -h $PATH_SRC HelloJNI.java

# Remove Cache
[ -f $PATH_CACHE ] && rm $PATH_CACHE

# Build
[ ! -d $PATH_BUILD ] && mkdir $PATH_BUILD

# build .so (cmake)
#cmake -S . -B $PATH_BUILD
#cmake --build $PATH_BUILD --config Release

# build .so (gcc)
ARCH="x86_64"
PATH_ARCH=$PATH_BUILD/$ARCH
PATH_LIB=$PATH_ARCH/libhello.so

# Arch
[ ! -d $PATH_ARCH ] && mkdir $PATH_ARCH

gcc -fPIC -I"$PATH_JAVA_HOME/include" -I"$PATH_JAVA_HOME/include/linux" -shared -o $PATH_LIB $PATH_CODE_C

# run with java
java -Djava.library.path=$PATH_ARCH HelloJNI
echo "Generated arch: $ARCH / path: $PATH_ARCH"

# build .so (x86)
ARCH="x86"
PATH_ARCH=$PATH_BUILD/$ARCH
PATH_LIB=$PATH_ARCH/libhello.so

# Arch
[ ! -d $PATH_ARCH ] && mkdir $PATH_ARCH

i686-linux-gnu-gcc-9 -fPIC -I"$PATH_JAVA_HOME/include" -I"$PATH_JAVA_HOME/include/linux" -shared -o $PATH_LIB $PATH_CODE_C
echo "Generated arch: $ARCH / path: $PATH_ARCH"

# build .so (armeabi-v7a)
ARCH="armeabi-v7a"
PATH_ARCH=$PATH_BUILD/$ARCH
PATH_LIB=$PATH_ARCH/libhello.so

# Arch
[ ! -d $PATH_ARCH ] && mkdir $PATH_ARCH
arm-linux-gnueabi-gcc -fPIC -I"$PATH_JAVA_HOME/include" -I"$PATH_JAVA_HOME/include/linux" -shared -o $PATH_LIB $PATH_CODE_C
echo "Generated arch: $ARCH / path: $PATH_ARCH"

# build .so (arm64-v8a)
ARCH="arm64-v8a"
PATH_ARCH=$PATH_BUILD/$ARCH
PATH_LIB=$PATH_ARCH/libhello.so

# Arch
[ ! -d $PATH_ARCH ] && mkdir $PATH_ARCH

aarch64-linux-gnu-gcc -fPIC -I"$PATH_JAVA_HOME/include" -I"$PATH_JAVA_HOME/include/linux" -shared -o $PATH_LIB $PATH_CODE_C
echo "Generated arch: $ARCH / path: $PATH_ARCH"

