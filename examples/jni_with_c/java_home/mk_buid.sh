#!/bin/bash

echo 'public class Main {public static void main(String[] args) {System.out.println(System.getProperty("java.home"));}}' > Main.java
javac Main.java
java Main

# Remove the files
[ -f Main.java ] && rm Main.java
[ -f Main.class ] && rm Main.class
