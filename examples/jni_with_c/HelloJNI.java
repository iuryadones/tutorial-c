public class HelloJNI {  // Save as HelloJNI.java
    static {
        // Load native library hello.dll (Windows) or libhello.so (Unixes)
        System.loadLibrary("hello"); 
        //  at runtime This library contains a native method called sayHello()
    }

    // Declare an instance native method sayHello() which receives no parameter
    // and returns void
    private native void sayHello();
    private native double sum(double x, double y);

    // Test Driver
    public static void main(String[] args) {

        // Create an instance and invoke the native method
        System.out.println(" - - - Method: sayHello()");
        new HelloJNI().sayHello();

        // Params
        double x_value = 6.54;
        double y_value = 0.46;
        double value_result;

        // Show the info of the sum
        System.out.println(" - - - Method: sum(double x, double y)");
        System.out.println("The x value is: " + x_value);
        System.out.println("The y value is: " + y_value);

        // Create an instance and use the native sum method
        HelloJNI hello_jni = new HelloJNI();

        //Compute the sum of two values
        value_result = hello_jni.sum(x_value, y_value);
        System.out.println("The number sum is: " + value_result);
    }
}
