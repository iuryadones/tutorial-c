#include<stdio.h>
#include<stdlib.h>


int main(void) {
    int vectorA[5];
    int *pointerB;

    pointerB = (int *)malloc(5*sizeof(int));
    vectorA[0] = 10;
    pointerB[0] = 10;
    printf("A[0] = %d\nB[0] = %d\n", vectorA[0], pointerB[0]);
    free(pointerB);
    return 0;
}
