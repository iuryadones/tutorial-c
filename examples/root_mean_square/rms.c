#include <stdio.h>
#include <math.h>


#define S 8
#define N 4

float* pow2(float* sample, int size);
float* pow2(float* sample, int size) {
    float *result;
    for (int i=0; i<size; i++) {
        result[i] = sample[i] * sample[i];
    }
    return result;
}

float* reduce_mean(float* sample, int size);
float* reduce_mean(float* sample, int size) {
    float *result;
    result[0] = 0;
    for (int i=0; i<size; i++) {
        result[0] += sample[i];
    }
    result[0] = result[0]/size;
    return result;
}

int main(void) {
    float signal[S] = {0.1, 0.5, 0.5, 0.3,
                       0.8, 0.5, 0.3, 0.2};
    float window[N];
    int j;
    int cnt = 0;
    for (int s=0; s<S; s++) {
        if (s%N == 0){
            j = 0;
            cnt += 1;
        }
        window[j] = signal[s];
        j++;
        if (j<N) {
            continue;
        }

        for (int i=0; i<N; i++)
            printf("%d - Window[%d]: %f\n", cnt, i, window[i]); 

        float* res_pow2;
        res_pow2 = pow2(window, N);
        for (int i=0; i<N; i++)
            printf("%d - Pow2[%d]: %f\n", cnt, i, res_pow2[i]); 

        float* res_mean;
        res_mean = reduce_mean(res_pow2, N);
        printf("%d - Mean: %f\n", cnt, res_mean[0]); 

        float res_sqrt;
        res_sqrt = sqrtf((float)res_mean[0]);
        printf("%d - Sqrt: %f\n", cnt, res_sqrt); 
    }

    return(0);
}
