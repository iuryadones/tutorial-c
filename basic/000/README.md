# Basic 000

## Install Code Compile

Install on the Ubuntu the C compile

```bash
$ sudo apt update
$ sudo apt install gcc
```

Install on the Termux the C compile

```bash
$ pkg update
$ pkg install gcc
```

## Build Binary Code

Use GCC on the Linux

```bash
$ gcc src/main.c -o bin/main
```

```bash
$ cd src
$ gcc main.c -o main.o
$ cd ..
```

## Run Binary

Run binary code

```bash
$ ./bin/main
Hello World!!!
$
```

```bash
$ cd src
$ ./main.o
Hello World!!!
$ cd ..
```

## Install Manual

On the Ubuntu

```bash
$ apt install man
$ man prinf
...
```

On the Termux 

```bash
$ pkg install man
$ man prinf
...
```

Obs: use `:q` to quit.

